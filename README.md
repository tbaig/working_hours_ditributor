# Expedition Scheduler CLI
THIS IS AN ELABORATE JOKE! IT WORKS BUT IT SHOULD NEVER BE USED BY ANYBODY. NO GUARANTEES ARE GIVEN
## Introduction

The Expedition Scheduler CLI is a Python-based command-line interface designed to assist in scheduling employee work periods. This tool ensures compliance with German labor laws, making it ideal for HR departments and team managers who need to adhere to legal standards regarding work hours and rest periods.

## Key Features

- **Compliance Ensured**: Adheres strictly to German labor laws, managing daily and weekly work hours, mandatory rest periods, and consecutive workday limits.
- **Flexible Scheduling**: Input custom start and end dates for planning employee work schedules.
- **Clear Outputs**: Outputs detailed schedules showing workdays, work hours, and mandatory breaks.

## Installation
Ensure you have Python 3.8 or newer and Poetry installed. Follow these steps to set up the tool:

1. **Clone the Repository**
   Clone the project repository using the following command:
   ```bash
   git clone https://gitlab.gwdg.de/tbaig/working_hours_ditributor.git
   cd working_hours_ditributo
   ```

2. **Install Dependencies Using Poetry**
   Install Poetry if you haven't already:
   ```bash
   pip install poetry
   ```
   Then, install the project dependencies:
   ```bash
   poetry install
   ```

3. **Activate the Poetry Shell**
   To activate the virtual environment managed by Poetry:
   ```bash
   poetry shell
   ```

This setup uses Poetry to manage dependencies and virtual environments, simplifying the development and deployment process.

## Dependencies
pytest for running the tests

## Usage

Use the CLI by providing a start date, end date, and total work hours. The dates must be in ISO format (YYYY-MM-DD).

### Command Line Arguments

- `start_date`: Start date for the work period (inclusive).
- `end_date`: End date for the work period (inclusive).
- `hours`: Total number of hours to distribute across the workdays.

### Example Command

Schedule 40 work hours from January 1, 2024, to January 10, 2024:
```bash
python scheduler.py 2024-01-01 2024-01-10 40
```

## Contributing

Contributions to the project are welcome. Follow these steps:

1. Fork the repository.
2. Create your feature branch (`git checkout -b feature-branch`).
3. Commit your changes (`git commit -am 'Add some feature'`).
4. Push to the branch (`git push origin feature-branch`).
5. Create a new Pull Request.

## License

This project is licensed under the MIT License - see the `LICENSE` file for more details.


