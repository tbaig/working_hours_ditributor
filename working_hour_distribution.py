import datetime
import dataclasses

from typing import List, Union


# Define data classes for work breaks, workdays, free days, and expeditions.
@dataclasses.dataclass
class WorkBreak:
    start: datetime.time
    end: datetime.time


@dataclasses.dataclass
class WorkDay:
    day: datetime.date
    work_start: datetime.time
    work_end: datetime.time
    breaks: List[WorkBreak]

    # Function to check if the day is Sunday
    def is_sunday(self):
        return self.day.weekday() == 6


@dataclasses.dataclass
class FreeDay:
    day: datetime.date
    freetime_start: datetime.time
    freetime_end: datetime.time


@dataclasses.dataclass
class Expedition:
    days: List[Union[WorkDay, FreeDay]]


# Helper function to calculate total hours worked in a day
def total_hours_worked(work_start, work_end, breaks):
    """Calculate the total hours worked excluding breaks."""
    work_duration = datetime.datetime.combine(datetime.date.min, work_end) - datetime.datetime.combine(
        datetime.date.min, work_start)
    for brk in breaks:
        break_duration = datetime.datetime.combine(datetime.date.min, brk.end) - datetime.datetime.combine(
            datetime.date.min, brk.start)
        work_duration -= break_duration
    return work_duration.total_seconds() / 3600


def check_compliance(expedition):
    total_week_hours = 0
    consecutive_workdays = 0
    last_end_time = None
    is_compliant = True
    error_messages = []

    for day in expedition.days:
        if isinstance(day, WorkDay):
            # Check for 11-hour rest period between workdays
            if last_end_time and (day.work_start < (
                    datetime.datetime.combine(day.day, last_end_time) + datetime.timedelta(hours=11)).time()):
                is_compliant = False
                error_messages.append(f'Violation on {day.day}: Insufficient rest period between workdays.')

            # Calculate total working hours for the day
            hours_worked = total_hours_worked(day.work_start, day.work_end, day.breaks)
            total_week_hours += hours_worked
            last_end_time = day.work_end

            # Increment consecutive workdays count
            consecutive_workdays += 1

            # Check if working hours exceed 10 hours (including up to 45 minutes of breaks)
            if hours_worked > 10:
                is_compliant = False
                error_messages.append(f'Violation on {day.day}: Worked more than 10 hours.')

            # Check for more than 12 consecutive workdays
            if consecutive_workdays > 12:
                is_compliant = False
                error_messages.append(f'Violation as of {day.day}: More than 12 consecutive workdays without a break.')

        elif isinstance(day, FreeDay):
            # Reset consecutive workday count
            consecutive_workdays = 0

        # Reset the weekly hours count after Sunday
        if day.day.weekday() == 0:  # Monday
            if total_week_hours > 48:
                is_compliant = False
                error_messages.append(
                    f'Violation in week starting {day.day - datetime.timedelta(days=1)}: Worked more than 48 hours last week.')
            total_week_hours = 0  # Reset for the new week

    return is_compliant, error_messages


def create_compliant_expedition_with_range(start_date, end_date, total_hours):
    days = []
    current_date = start_date
    daily_hours = min(8, total_hours)  # Assume a normal working day is 8 hours maximum for compliance
    consecutive_workdays = 0

    while current_date <= end_date and total_hours > 0:
        if current_date.weekday() < 6:  # Skip Sundays
            if consecutive_workdays < 12:
                # Calculate work hours for the day
                work_hours_today = min(daily_hours, total_hours)
                start_time = datetime.time(9, 0)
                end_time = (datetime.datetime.combine(datetime.date.min, start_time) + datetime.timedelta(
                    hours=work_hours_today)).time()

                if work_hours_today > 6:
                    breaks = [WorkBreak(start=datetime.time(12, 0), end=datetime.time(12, 30))]
                else:
                    breaks = []

                workday = WorkDay(day=current_date, work_start=start_time, work_end=end_time, breaks=breaks)
                days.append(workday)
                total_hours -= work_hours_today
                consecutive_workdays += 1
            else:
                # Insert a FreeDay to break the sequence of WorkDays
                freeday = FreeDay(day=current_date, freetime_start=datetime.time(0, 0),
                                  freetime_end=datetime.time(23, 59))
                days.append(freeday)
                consecutive_workdays = 0  # Reset consecutive workdays count
        else:
            # Always add FreeDay on Sunday
            freeday = FreeDay(day=current_date, freetime_start=datetime.time(0, 0), freetime_end=datetime.time(23, 59))
            days.append(freeday)
            consecutive_workdays = 0  # Reset consecutive workdays count

        # Move to the next day
        current_date += datetime.timedelta(days=1)

    if total_hours > 0:
        raise ValueError("Not enough valid days to schedule all work hours within the given range.")

    expedition = Expedition(days=days)

    # Check compliance
    compliant, messages = check_compliance(expedition)
    if not compliant:
        raise ValueError(f"Could not create a compliant expedition: {', '.join(messages)}")

    return expedition
