import argparse
import datetime
from datetime import datetime as dt

from working_hour_distribution import create_compliant_expedition_with_range, WorkDay, FreeDay


# Assuming previous definitions are imported here such as:
# from your_module import create_compliant_expedition_with_range, Expedition, WorkDay, FreeDay

def main():
    # Set up the command line argument parser
    parser = argparse.ArgumentParser(description="Generate a compliant Expedition based on provided work hours and date range.")
    parser.add_argument('start_date', type=str, help='Start date in ISO format (YYYY-MM-DD)')
    parser.add_argument('end_date', type=str, help='End date in ISO format (YYYY-MM-DD)')
    parser.add_argument('hours', type=int, help='Total work hours to be scheduled within the date range')


    # Parse the arguments
    args = parser.parse_args()

    # Convert ISO date strings to date objects
    start_date = datetime.date.fromisoformat(args.start_date)
    end_date = datetime.date.fromisoformat(args.end_date)
    total_hours = args.hours

    try:
        # Create a compliant expedition
        expedition = create_compliant_expedition_with_range(start_date, end_date, total_hours)
        # Print the expedition details in a human-readable format
        print_expedition(expedition)
    except ValueError as e:
        print(f"Error: {e}")

def print_expedition(expedition):
    """
    Prints the details of the expedition in a human-readable format.
    """
    for day in expedition.days:
        if isinstance(day, WorkDay):
            print(f"WorkDay: {day.day} - Start: {day.work_start}, End: {day.work_end}")
            for brk in day.breaks:
                print(f"  Break: {brk.start} to {brk.end}")
        elif isinstance(day, FreeDay):
            print(f"FreeDay: {day.day} - Free from: {day.freetime_start} to {day.freetime_end}")

if __name__ == "__main__":
    main()
