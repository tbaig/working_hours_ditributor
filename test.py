import datetime

import pytest

from cli import print_expedition
from working_hour_distribution import Expedition, WorkBreak, WorkDay, FreeDay, check_compliance, \
    create_compliant_expedition_with_range


def test_compliange():
    # Example usage of the compliance checker
    expedition = Expedition(days=[
        WorkDay(day=datetime.date(2024, 6, 17), work_start=datetime.time(8, 0), work_end=datetime.time(19, 0),
                breaks=[WorkBreak(start=datetime.time(12, 0), end=datetime.time(12, 30))]),
        FreeDay(day=datetime.date(2024, 6, 18), freetime_start=datetime.time(0, 0), freetime_end=datetime.time(23, 59))
    ])

    compliant, messages = check_compliance(expedition)
    print(f"Is the schedule compliant? {compliant}")
    if not compliant:
        for message in messages:
            print(message)
    assert compliant is False

def test_expedition_within_date_range():
    start_date = datetime.date(2024, 6, 1)  # Start on a Saturday
    end_date = datetime.date(2024, 6, 10)  # End on a Monday
    total_hours = 16  # 16 hours to be scheduled

    expedition = create_compliant_expedition_with_range(start_date, end_date, total_hours)
    print_expedition(expedition)
    assert len(expedition.days) ==  3, "Should schedule work across two working days only"

    compliant, messages = check_compliance(expedition)
    assert compliant, f"Expedition should be compliant but found issues: {messages}"